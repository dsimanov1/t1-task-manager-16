package ru.t1.simanov.tm.service;

import ru.t1.simanov.tm.api.repository.IProjectRepository;
import ru.t1.simanov.tm.api.repository.ITaskRepository;
import ru.t1.simanov.tm.api.service.IProjectTaskService;
import ru.t1.simanov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.simanov.tm.exception.entity.TaskNotFoundException;
import ru.t1.simanov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.simanov.tm.exception.field.TaskIdEmptyException;
import ru.t1.simanov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);

    }

    @Override
    public void unbindTaskFromProject(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
